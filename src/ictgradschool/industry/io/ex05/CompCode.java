package ictgradschool.industry.io.ex05;

public class CompCode extends CodeNum {
    // field declarations
    // has int[] codenumber from superclass


    //constructor:
    public CompCode(){

        boolean gotValidCode = false;
        while (!gotValidCode) {
            //make up a temporary code and if it's valid, set it as the proper robotcode
            int[] temp = randomify();
            if (checkValid(temp)) {
                for (int i = 0; i < codeNumber.length; i++) {
                    codeNumber[i] = temp [i];
                }
                return;
            }
        }
    }

    public int[] randomify(){ //returns a random array length 4
        int[] random = new int[4];
        for (int i = 0; i < random.length; i++) {
            random[i] = (int)(Math.random() *10);
        }
        return random;
    }

}
