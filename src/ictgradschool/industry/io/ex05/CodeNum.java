package ictgradschool.industry.io.ex05;

public abstract class CodeNum {

    int[] codeNumber = new int[4];

    // a method to check if a CodeNumber object is valid ie contains unique digits
    boolean checkValid(int[] toCheck){

        for (int i = 0; i < toCheck.length ; i++) {
            int occurrence = 0;
            for (int j = 0; j < toCheck.length ; j++) {
                if (toCheck[i] == toCheck [j]){
                    occurrence++;
                    if (occurrence > 1){
                        return false;
                    }
                }
            }
        }
        return true;
    }


    // an override to print out CodeNumber objects
    public String toString(){
        String str = "";
        int[] x = this.codeNumber;
        for (int i = 0; i < x.length; i++) {
            str += x[i];
        }
        return str;
    }

    public int[] getArray(){
        return codeNumber;
    }


    public String toString(int[] array){
        String str = "";
        for (int i = 0; i < array.length; i++) {
            str += array[i];
        }
        return str;
    }

    /// the method which compares the two numbers and gets the bulls and cows
    public int[] compare(CodeNum target) {
        int bulls = 0;
        int cows = 0;
        int[] targetA = target.getArray();
        int[] guessA = this.getArray();

        for (int i = 0; i < targetA.length; i++) {
            if (targetA[i] == guessA[i]) {
                bulls++;
            }
            for (int i1 = 0; i1 < guessA.length; i1++) {
                if (targetA[i] == guessA[i1] && i != i1) {
                    cows++;
                }
            }
        }
        return new int[]{bulls, cows};
        /// will return an int[] with [0] == bulls, [1]== cows
    }

}


