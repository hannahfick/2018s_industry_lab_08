package ictgradschool.industry.io.ex05;

import ictgradschool.Keyboard;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Game {

    //should i have all these variables just hanging out here??? I managed to decrease the number at least
    private UserCode secret;
    private CompCode target;
    private List guesses = new ArrayList();
    private gameWriter myFile = null;
    private int iterations = 0;

    //todo does this constructor actually do anything?????
    private Game() {
    }


    public void start() {
        // does the intro
        Boolean auto = intro();
//       asks to save or not
        toSaveOrNotToSave(auto);

        //starts game play
        gamePlay();
    }


    // starts the game and returns a boolean if the user will guess manually
    private Boolean intro() {
        System.out.println("----- Welcome to ~Cows and Bulls~ -----");
        secret = getUserCode("secret code", guesses, iterations, myFile);
        this.target = getCompCode();


        boolean auto = false;
        String response = "";
        while (!response.trim().equals("Y") && !response.trim().equals("y") && !response.trim().equals("N") && !response.trim().equals("n")) {

            System.out.println("Guess automatically? Y/N: ");
            response = Keyboard.readInput();
            if (response.trim().equals("Y") || response.trim().equals("y")) {

                guesses = autoGuess();
                auto = true;
            } else if (response.trim().equals("N") || response.trim().equals("n")) {
                auto = false;

            } else {
                System.out.println("??? Try again weirdo???");
            }
        }
        return auto;
    }



    private List autoGuess() {

        System.out.println("Please enter the filename: ");
        String userInput = Keyboard.readInput();
        File guessFile = new File(userInput);
        List<String> leGuesses = new ArrayList<>();
        while (!guessFile.exists()) {
            System.out.println("File doesn't exist. Try again: ");
            userInput = Keyboard.readInput();
            guessFile = new File(userInput);
        }
        String x;
        try (BufferedReader reader = new BufferedReader(new FileReader(guessFile))) {

            while ((x = reader.readLine()) != null) {
                leGuesses.add(x);
            }
        } catch (IOException e) {
            System.out.println("IO exception " + e);
        }

        return leGuesses;
    }


//    took ten thousand times to get this.... probably still a better way to do it.


    private void toSaveOrNotToSave(Boolean auto) {
        String saveResponse = "";
        while (!saveResponse.trim().equals("Y") && !saveResponse.trim().equals("y") && !saveResponse.trim().equals("N") && !saveResponse.trim().equals("n")) {

            System.out.println("Save game results to file? Y/N: ");
            saveResponse = Keyboard.readInput();
            if (saveResponse.trim().equals("Y") || saveResponse.trim().equals("y")) {
                System.out.println("Name your file: ");
                String fileName = Keyboard.readInput();
                System.out.println("Save location: ");
                String filePath = Keyboard.readInput();


                myFile = new gameWriter(filePath, fileName);


                myFile.writeData("COWS AND BULLS RESULTS");
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                String date = formatter.format(new Date());
                myFile.writeData(date);
                myFile.writeData("-----------------------------------");
                myFile.writeData("User's secret code is: " + secret);

                if (auto) {
                    myFile.writeData("User chose to guess automatically.");
                }
                myFile.writeData("-----------------------------------");
            } else if (saveResponse.trim().equals("N") || saveResponse.trim().equals("n")) {

            } else {
                System.out.println("???");
            }
        }
    }


    private void gamePlay() {

        boolean finished = false;
        //get the user's guess and check it, get the comp's guess and check it
        while (!finished && iterations < 7) {
            finished = usersTurn();
            if (finished) {
                break;
            }
            finished = robotTurn();
            if (finished) {
                break;
            }
            iterations++;

            System.out.println("Guesses remaining: " + (7 - iterations));
            if (myFile != null) {
                myFile.writeData("Guesses remaining: " + (7 - iterations));
            }
            if (7 - iterations == 0) {
                System.out.println("Out of guesses xxx RIP xxx");
                System.out.println("Tie!");
                if (myFile != null) {
                    myFile.writeData("Out of guesses xxx RIP xxx");
                    myFile.writeData("Tie!");
                }
            }
            System.out.println("-----------------");
            if (myFile != null) {
                myFile.writeData("-----------------");
            }
        }

        System.out.println("Game over");
        if (myFile != null) {
            myFile.writeData("Game over");
        }

    }


    private boolean usersTurn() {
        UserCode guess = getUserCode("guess", guesses, iterations, myFile);
        int[] results = guess.compare(target);
        System.out.println("Result: " + results[0] + " bulls and " + results[1] + " cows.");
        if (myFile != null) {
            myFile.writeData("Result: " + results[0] + " bulls and " + results[1] + " cows.");
        }

        if (results[0] == 4) {
            System.out.println("\\-----------");
            System.out.println("!You win!");
            System.out.println("-----------\\");
            if (myFile != null) {
                myFile.writeData("\\-----------");
                myFile.writeData("!You win!");
                myFile.writeData("-----------\\");
            }
            return true;
        }
        return false;
    }


    private boolean robotTurn() {
        System.out.println();
        CompCode rguess = getCompCode();
        System.out.println("Computer's guess: " + rguess);
        if (myFile != null) {
            myFile.writeData("Computer's guess: " + rguess);
        }
        int[] rResults = rguess.compare(secret);
        System.out.println("Result: " + rResults[0] + " bulls and " + rResults[1] + " cows.");
        if (myFile != null) {
            myFile.writeData("Result: " + rResults[0] + " bulls and " + rResults[1] + " cows.");
        }
        if (rResults[0] == 4) {
            System.out.println("The computer wins");
            if (myFile != null) {
                myFile.writeData("The computer wins");
            }
            return true;
        }
        return false;
    }


    private CompCode getCompCode() {
        return new CompCode();
    }


    private UserCode getUserCode(String type, List guesses, int iterations, gameWriter myFile) {

        UserCode secret = new UserCode(type, guesses, iterations);
        System.out.println("Your " + type + " is: " + secret);
        if (myFile != null) {
            myFile.writeData("Your " + type + " is: " + secret);
        }

        return secret;
    }


    public static void main(String[] args) {
        Game game = new Game();
        game.start();
    }

}