package ictgradschool.industry.io.ex04;

import ictgradschool.industry.io.ex03.Movie;
import ictgradschool.industry.io.ex03.MovieReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieReader extends MovieReader {

    @Override
    protected Movie[] loadMovies(String fileName) {

        // TODO Implement this with a Scanner

        File newFile = new File (fileName);

        Movie[] films = new Movie[19];

        try (Scanner scanner = new Scanner (newFile)){

        scanner.useDelimiter(",|\\n");

        int i = 0;

        while (scanner.hasNext()){

            String name = scanner.next();
            int year = scanner.nextInt();
            int length = scanner.nextInt();
            String director = scanner.next();

            films[i] = new Movie (name,year,length,director);
            i++;

        }



        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        return films;
    }

    public static void main(String[] args) throws IOException {
        new Ex4MovieReader().start();
    }
}
