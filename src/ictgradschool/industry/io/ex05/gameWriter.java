package ictgradschool.industry.io.ex05;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class gameWriter {
    File myFile;
    String filePath;
    String fileName;

    public gameWriter(String filePath, String fileName){
        this.filePath = filePath;
        this.fileName = fileName;

    }


    public void writeData(String data){

        File myFolder = new File(this.filePath);
        if (!myFolder.exists()){
            myFolder.mkdirs();
        }
        if(filePath == null){
            myFile = new File(fileName);
        }else {
            myFile = new File(myFolder, fileName);
        }
        //System.out.println("save file = " + myFile.toString());
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(myFile, true))){
            writer.write(data);
            writer.newLine();

        } catch (IOException e){
            System.out.println("problem with the file writing");
        }


    }
}
