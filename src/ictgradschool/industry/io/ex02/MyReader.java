package ictgradschool.industry.io.ex02;

import ictgradschool.Keyboard;

import java.io.*;

public class MyReader {

    public void start() {

        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();
        File handle = new File(fileName);


        try (BufferedReader br = new BufferedReader(new FileReader(handle))) {

            String line = null;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }

        } catch (FileNotFoundException e) {
            System.out.println("File not found problem" + e);
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("IO problem" + e);
        }
    }

    public static void main(String[] args) {
        new MyReader().start();
    }
}

