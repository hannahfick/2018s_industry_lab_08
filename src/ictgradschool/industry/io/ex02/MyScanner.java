package ictgradschool.industry.io.ex02;

import ictgradschool.Keyboard;

import java.io.*;
import java.util.Scanner;

public class MyScanner {

    public void start() {

        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();
        File handle = new File(fileName);

        try (Scanner scanner = new Scanner(handle)){

            while (scanner.hasNextLine()) {
                System.out.println(scanner.nextLine());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a Scanner.
    }

    public static void main(String[] args) {
        new MyScanner().start();
    }
}

