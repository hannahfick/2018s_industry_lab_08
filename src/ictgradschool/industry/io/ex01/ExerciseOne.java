package ictgradschool.industry.io.ex01;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ExerciseOne {

    public void start() {

        printNumEsWithFileReader();

        printNumEsWithBufferedReader();

    }

    private void printNumEsWithFileReader() {

        int numE = 0;
        int total = 0;

        try (FileReader fR = new FileReader("input2.txt")) {
            int read_char;
            while ((read_char = fR.read()) != -1) {
                total++;
                if ((read_char == 'e' || read_char == 'E')) {
                    numE++;
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found problem" + e);
        } catch (IOException e) {
            System.out.println("IO problem" + e);
        }
        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    private void printNumEsWithBufferedReader() {

        int numE = 0;
        int total = 0;
        try (BufferedReader reader = new BufferedReader(new FileReader("input2.txt"))) {
            String line = null;
            while ((line = reader.readLine()) != null) {

                for (int i = 0; i < line.length(); i++){
                    total++;
                    if (line.toLowerCase().charAt(i) == 'e') {
                        numE++;
                    }
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found problem" + e);
        } catch (IOException e) {
            System.out.println("IO problem" + e);
        }

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a BufferedReader.

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    public static void main(String[] args) {
        new ExerciseOne().start();
    }

}




